import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MemberGuard } from './modules/shared-member/guards/member.guard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WindowProviderModule } from './global/window.provider.module';
import { environment } from 'src/environments/environment';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'basic' }),
    BrowserAnimationsModule,
    HammerModule,
    AppRoutingModule,
    WindowProviderModule.forRoot(),
  ],
  providers: [MemberGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
