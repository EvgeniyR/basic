import { Component, OnInit } from '@angular/core';
import { tap } from 'rxjs/operators';
import { LoggedUser } from 'src/app/modules/auth/services/user-login-response/user-login-response.model';
import { PlatformService } from 'src/app/global/platform/platform.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'evh-public-header',
  templateUrl: './public-header.component.html',
  styleUrls: ['./public-header.component.scss']
})
export class PublicHeaderComponent implements OnInit {

  public userAuthData: LoggedUser;
  public userAuthData$: Observable<any>;
  constructor(private platformService: PlatformService) { }

  ngOnInit() {
    if (this.platformService.isBrowser()) {
    }
  }

  /**
   * login with Auth0
   */
  public login() {
  }

  public logoutUser(): void {
  }
}
