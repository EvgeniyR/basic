import { MenuStates } from './menu-states.enum';

export type MenuStateTypes = MenuStates.PENDING | MenuStates.OPEN | MenuStates.CLOSED;
