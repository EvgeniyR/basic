export enum MenuStates {
  PENDING = 'pending',
  OPEN = 'open',
  CLOSED = 'closed'
}
