import { Component, OnInit } from '@angular/core';
import { takeUntil, tap } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { LoggedUser } from '../../auth/services/user-login-response/user-login-response.model';
import { PlatformService } from 'src/app/global/platform/platform.service';

@Component({
  selector: 'evh-account-member-menu',
  templateUrl: './account-member-menu.component.html',
  styleUrls: ['./account-member-menu.component.scss']
})
export class AccountMemberMenuComponent implements OnInit {

  public profile$: Observable<LoggedUser>;
  public menuButtonStyleConfig: { [key: string]: string };
  private onDestroy$: Subject<boolean> = new Subject();

  constructor(private platformService: PlatformService) {

   }

  ngOnInit(): void {
    if (this.platformService.isBrowser()) {
    }
  }

  public accountLogout(): void {
  }

}
