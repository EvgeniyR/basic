import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, of } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { PlatformService } from 'src/app/global/platform/platform.service';

@Injectable()
export class MemberGuard implements CanActivate {

  constructor(private platformService: PlatformService) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> {
    if (this.platformService.isBrowser()) {
      return of(true);
    }
  }

}
