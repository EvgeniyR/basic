import { Component, OnInit } from '@angular/core';
import { PlatformService } from './global/platform/platform.service';

@Component({
  selector: 'evh-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private platformService: PlatformService) {
  }

  ngOnInit(): void {
    if (this.platformService.isBrowser()) {
    }
  }
}
